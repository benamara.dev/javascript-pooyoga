/*
1- convertir les classes, balises ... en variable
2- stocker l'APP dans une variable de tableau
3- declarer une classe pour creer mes objets
4- creer un objet pour mettre toutes les fonctions utile
    4-1: creer une methode "pageContent"
    4-2: creer un evenement pour injecter les minutes
    4-3: creer un evenement pour gerer les arrow "handleEventArrow"
    4-4: intervertir les objet dans le tableau "exerciceArray"
    4-5: creer un evenement pour supprimer les card "handleEventArrow"
    4-6: creer un evenement de reboot
        4-6-1 : tableau de reboot
        4-6-2 : local storage / JSON

5- creer un objet pour naviguer entre mes pages
    5-1: Injecter du HTML
    5-2: je convertis la valeur de "content" en variable "mapArray"
    5-3: injecter l evenement
    --


*/


const main = document.querySelector("main");

//---------------------------------------------------
const basicArray = [
  { pic: 0, min: 1 },
  { pic: 1, min: 1 },
  { pic: 2, min: 1 },
  { pic: 3, min: 1 },
  { pic: 4, min: 1 },
  { pic: 5, min: 1 },
  { pic: 6, min: 1 },
  { pic: 7, min: 1 },
];
let exerciceArray = [];

// Get stored exercices array
(() => {
    if (localStorage.exercices) {
        exerciceArray = JSON.parse(localStorage.exercices);
    } else {
        exerciceArray = basicArray;
    }
})();
//--------------------------------------------
class Exercice {
    constructor(){
        this.index = 0;
        this.minutes = exerciceArray[this.index].min;
        this.seconds = 0;
    }
    updateCountdown(){
       this.seconds = this.seconds < 10 ? "0" + this.seconds : this.seconds;

       setTimeout(() => {
        if (this.minutes === 0 && this.seconds === "00") {
          this.index++;
          this.ring();
          if (this.index < exerciceArray.length) {
            this.minutes = exerciceArray[this.index].min;
            this.seconds = 0;
            this.updateCountdown();
          } else {
            return page.finish();
          }
        } else if (this.seconds === "00") {
          this.minutes--;
          this.seconds = 59;
          this.updateCountdown();
        } else {
          this.seconds--;
          this.updateCountdown();
        }
      }, 10);

        return(main.innerHTML = 
            `
            <div class="exercice-container">

                <p>${this.minutes}:${this.seconds}</p>
                <img src="images/${exerciceArray[this.index].pic}.jpg" />
                <div>${this.index + 1}/${exerciceArray.length} </div>

            </div>
        `);
    }
    ring() {
        const audio = new Audio();
        audio.src = "images/ring.mp3";
        audio.play();
    }
}
//-------------------------------------------
const utils = {
    pageContent: function (title, content, btn) {
        const h1 = document.querySelector("h1");
        const btnContainer = document.querySelector(".btn-container");

        h1.innerHTML = title;
        main.innerHTML = content;
        btnContainer.innerHTML = btn;
    },

    handleEventMinutes: function () {
        const inputAll = document.querySelectorAll('input[type="number"]');

        inputAll.forEach((input) => {
        input.addEventListener("input", (e) => {
            exerciceArray.map((exo) => {
            if (exo.pic == e.target.id) {
                exo.min = parseInt(e.target.value);
                this.handleEventStore();
            }
            });
        });
        });
    },

    handleEventArrow: function () {
        const arrowAll = document.querySelectorAll(".arrow");

        arrowAll.forEach((arrow) => {
        arrow.addEventListener("click", (e) => {
            let position = 0;
            exerciceArray.map((exo) => {
            if (exo.pic == e.target.dataset.pic && position !== 0) {
                [exerciceArray[position], exerciceArray[position - 1]] = [
                exerciceArray[position - 1],
                exerciceArray[position],
                ];
                page.lobby();
                this.handleEventStore();
            } else {
                position++;
            }
            });
        });
        });
    },

    handleEventDelete: function () {
        const deleteBtn =  document.querySelectorAll(".deleteBtn");

        deleteBtn.forEach((btn) => {
        btn.addEventListener("click", (e) => {
            let newArr = [];
            exerciceArray.map((exo) => {
            if (exo.pic != e.target.dataset.pic) {
                newArr.push(exo);
            }
            });
            exerciceArray = newArr;
            page.lobby();
            this.handleEventStore();
        });
        });
    },

    handleEventReboot: function () {
        exerciceArray = basicArray;
        page.lobby();
        this.handleEventStore();
    },

    handleEventStore: function () {
        localStorage.exercices = JSON.stringify(exerciceArray);
    },
};

//-----------------------------------------
const page = {
    lobby: function () {
        let mapArray = exerciceArray
        .map(
            (exo) =>
            `
                <li>
                    <div class="card-header">
                        <input type="number" id=${exo.pic} min="1" max="10" value=${exo.min}>
                        <span>min</span>
                    </div>
                    <img src="images/${exo.pic}.jpg" />
                    <i class="fas fa-arrow-alt-circle-left arrow" data-pic=${exo.pic}></i>
                    <i class="fas fa-times-circle deleteBtn" data-pic=${exo.pic}></i>
                </li>
            `
        )
        .join("");

        utils.pageContent(
            "Paramétrage <i id='reboot' class='fas fa-undo'></i>",
            "<ul>" + mapArray + "</ul>",
            "<button id='start'>Commencer<i class='far fa-play-circle'></i></button>"
        );
        utils.handleEventMinutes();
        utils.handleEventArrow();
        utils.handleEventDelete();
        reboot.addEventListener("click", () => utils.handleEventReboot());
        start.addEventListener("click", () => this.routine()); //vue routine

    },
    routine : function () {
        const exercice = new Exercice(); // le resultat de Exercice() sera injecté dans le "content" --> voir en haut: class Exercice {}






        (utils.pageContent(
            "Routine",
            exercice.updateCountdown(),
            null
        ));
    },
    finish : function () {
        (utils.pageContent(
            "C'est terminé",
            "<button id='start'>Recommencer</button> ",
            "<button id='reboot' class='btn-reboot'>Réinitialiser <i class='fas fa-times-circle'></i> </button> "
        ));
        start.addEventListener("click", () => this.routine());
        reboot.addEventListener("click", () => utils.handleEventReboot());
    }
};

page.lobby();
//page.routine()
//page.finish()
